from sklearn.metrics import *
from sklearn.metrics import cohen_kappa_score
import pandas as pd
import numpy as np

'''
f1_scorevalue(actualclass, predictedclass)
Descr:  f1_score is weighted average of precision and recall ( Used only for binary classification)
        precision: number of correct positive results/ number of all positive results (TP/(TP+FP))
Recall: number of correct positive results/ number of positive results that should have been returned.
        (tp/(tp+fn))
Input:  Actual list of class values, predicted class values
Output: f1_ score in the range of 0 to 1, float value.
source: http://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html
'''
def f1_scorevalue(actualclass, predictedclass):
    f1score = f1_score(actualclass, predictedclass)
    print(f1score)

'''
EXPLANATION: kappa_statistics:-
Descr:  Calculates level of agreement between predicted and actual label. Used for classification problem.
Input:  Actual class values and predicted class values.
Output: Score in the range of 0 and 1. Float value.
source: http://scikit-learn.org/stable/modules/generated/sklearn.metrics.cohen_kappa_score.html
'''
def kappa_statisticsvalue(actualclass, predictedclass):
    kappascore = cohen_kappa_score(actualclass, predictedclass)
    print(kappascore)
'''
Function Name: r_square(actualclass, predictedclass)
Descr:  R square: Coefficient of determination
        Square of linear correlation coefficient of actual values and predicted values.
Input:  Actual class values and predicted class values.
Output: Score in the range of 0 and 1. Numeric values.
source: http://scikit-learn.org/stable/modules/generated/sklearn.metrics.r2_score.html
'''
def r_square(actualclass, predictedclass):
    r2score = r2_score(actualclass, predictedclass)
    print(r2score)

'''
Function name: confusion_matrixresults(actualclass, predictedclass)
Descr:  To check the accuracy of the model.
        Lists the classified and misclassified values in a matrix format.
        Lesser the misclassified values, better the prediction is.
Input:  Actual class labels and predicted class labels.
Output: Matrix form representing classified and misclassified values in numeric format.
'''
def confusion_matrixresults(actualclass, predictedclass):
    cf = confusion_matrix(actualclass, predictedclass)
    print (cf)

'''
Function name:  areaundercurve(actualclass, predictedclass)
Descr:  Another metric to calculate accuracy which determines by the area under the curve.
        More area under curve, better the prediction is.
Input:  actual class values and predicted class values.
Output: Float value in the range of 0 and 1.
'''
def areaundercurve(actualclass, predictedclass):
    value = roc_auc_score(actualclass, predictedclass)
    print(value)


if __name__ == "__main__":
    f1_scorevalue()
    kappa_statisticsvalue()
    r_square()
    confusion_matrixresults()
    areaundercurve()