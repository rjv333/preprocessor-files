import pandas as pd
import numpy as np
import pylab as plab
import matplotlib.pyplot as plt
import sys
from matplotlib.backends.backend_pdf import PdfPages
import math

reload(sys)
sys.setdefaultencoding("utf-8")

#path = 'E:/Datanomers/work/results/'
path = ""
def initial_diagnostics(filename):
    # try:
    pda = pd.read_csv(filename, delimiter = '\t')
    #pda = pd.read_csv('sample_amelia.txt',delimiter = '\t')
    pdacopy = pda
    pdacopy =pdacopy.dropna()
    # header stores the column names of all the columns of the data.
    header = []
    for column in pda:
        header.append(column)

    # Pandas dataframe converted to matrix.
    matform = np.asmatrix(pda)
    row,col = matform.shape
    features = matform[:,0:col-1]
    classval = matform[:,col-1].tolist()
    pp = PdfPages("diagnosticplots.pdf")
    # Call the methods.
    datatypes(pda)  # task 1
    shapeofdata(row, col) #Task 2
    #calculatevariance()
    findfrequency(row, col,matform, header, pp) #task 3
    pp.close()
    plt.close('all')

# TASK 1:
# data type of each column.
def datatypes(pda):
    try:
        # IMPORTANT: WRITE OUTPUT TO PDF
        dtypevalues = pda.dtypes
        print(dtypevalues)
        reportdtype = open(path + 'dtypereport.txt','wb')
        reportdtype.write(str(dtypevalues))
        reportdtype.close()
    except:
        # REPORT
        e = sys.exc_info()[0]
        errordtype = open(path + 'ERR_Dtype.txt','wb')
        errordtype.write(str(e))


# TASK 2:
# IMPORTANT: SHAPE OF THE DATA
# IF SHAPE IS IRREGULAR REPORT AND ABORT. (abort: sys.exit())
# row, col = p
def shapeofdata(row, col):
    try:
        dimension =[row, col]
        #print(dimension)
        reportshape = open(path + 'DimensionofData.txt','wb')
        reportshape.write(str(dimension))
        reportshape.close()
    except:
        e = sys.exc_info()[0]
        errorshape = open(path + 'ERR_dimension.txt','wb')
        errorshape.write(str(e))
        sys.exit(0)

# TASK3:
# IMPORTANT: HISTOGRAM
# calculating variance of each column.
# Error: Since few columns contain categorical(String) values. This will be dealt with post Saturday's meeting.
def findfrequency(row, col, mat, header, pp):
    count = 0
    for eachcol in range(col):
        frequency = {}
        dtype = ''
        iter = 0
        for value in mat[:,eachcol]:
            value = value.tolist()
            for v in value:
                if iter is 0:
                    for a in v:
                        dtype = type(a)
                        if a in frequency:
                            frequency[a] +=1
                        else:
                            frequency[a] = 1

        FeatureName = header[count]
        count += 1
        #print(FeatureName, frequency)

        if dtype == long or dtype == int or dtype == float:
            plthist(FeatureName, frequency, pp)
        else:
            histogram(FeatureName, frequency, pp)

def histogram(featurename, frequency, pp):
    #print("inside histogram")
    length = np.arange(len(frequency))
    lvalues = []
    lkeys = []
    for k, v in frequency.iteritems():
        if v > 1:
            lvalues.append(v)
            lkeys.append(k)

    #Maximum value of y axis is max value of dictionary added to 50.
    if max(frequency.values()) < 50:
        max_Value = max(frequency.values()) * 2
    else:
        max_Value = max(frequency.values()) + 100

    plab.figure()
    plab.bar(length, frequency.values(), width=1.0, align='center')
    #x axis has the keys in the dictionary.

    plab.xticks(length, frequency.keys())
    plab.xlabel(featurename)

    plab.ylabel('Frequency')
    plab.ylim(0, max_Value)
    plab.title('Histogram for ' +featurename)
    pp.savefig()

def plthist(featurename, frequency, pp):
    #print("inside plthist")
    #print frequency.keys()
    # plt.hist(frequency, bins=range(min(frequency), max(frequency)))
    # plt.bar(list(frequency.keys()), frequency.values())
    plt.figure()
    #print max(frequency.keys()), min(frequency.keys())
    #print frequency.keys()
    #if float('NaN') in frequency.keys():
    #    print "Here"
    #   keys = frequency.keys().remove(float('NaN'))
    #else:
    #    keys = frequency.keys()
    #print keys
    keys = [x for x in frequency.keys() if str(x) != 'nan']
    plt.hist(keys)
    plt.ylabel('Frequency')
    plt.xlabel(featurename)

    # plt.yticks(np.arange(10))
    plt.title('Histogram for '+featurename)
    pp.savefig()

if __name__ == "__main__":
    read_File()