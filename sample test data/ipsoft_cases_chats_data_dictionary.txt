
The dataset 'ipsoft_cases_to_chat_info.csv' was built by linking our sampled cases from one week to
the corresponding call center chat information.


The columns are:

nucleus_id: same

account_key: same

case_key: same

lp_chat_header_key: same

session_id: same

case_nbr: same

created_gmt_dttm_dt: date of case creation

close_gmt_dttm_dt: day when case was closed

subject: customer's words for the issue they are having

ctgry_key: same

product_key: same

ctgry_name: same


-- the above are the same definitions from our sampled_cases dataset --

contact_chat_key: key for the contact chat

realtime_session_id: session id for the chat

adv_chat_start_tms_cst_dttm: timestamp for when advisor starts the chat with the customer

adv_chat_end_.....  timestamp for when advisor ends the chat with the customer


speed_of_answer_time: time (in seconds) it takes for an advisor to pick up a chat with customer

handle_time: time in seconds the customer is in chat with the advisor

acw: tbd

order_of_rep: order of advisor - e.g. calls can get escalated so order_of_rep = 2 means it's the second rep to talk to the customer (don't think
this showed up correctly for Amelia in the first pilot)

wait_time: total time (in seconds the customer waited) - in general this is equal to speed_of_answer (almost everywhere I've found, I can't say with 100% that
there is never a difference between the two, but generally I haven't found one)

geo_country: geo location of the customer as determined by liveperson

geo_ip: ip address of customer

geo_isp: ISP of customer

geo_lat: lattitude of customer

geo_long: longitude of customer

dw_row_..... when row was created in data warehouse (unimportant)

etl_batch_id: id of etl batch

is_valid_flag: dw flag for valid rows (should all / mostly be 'Y')





























