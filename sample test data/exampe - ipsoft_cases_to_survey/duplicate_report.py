import pandas
import numpy as np
from collections import defaultdict
from collections import Counter
import sys
from pyPdf import PdfFileWriter
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib import colors


class duplicate_info:
    """
    CONSTRUCTOR
    Descr: Initializes attributes of 'duplicate_info' class
    """
    def __init__(self):
        # dataframe of input
        self.df = None
        # dataframe of duplicate records
        self.dup_rows = None
        # number of total rows in input
        self.total_rows = 0
        # sum of all rows with repeated instances
        self.num_dup_rows = 0
        # percentage of records with duplicates
        self.percentage_duplicate = 0
        # threshold of duplicate repeats
        self.percent_threshold = .4
        # collection of distinct observations
        self.duplicate_records = set()
        # mapping of duplicate record to number of observations
        self.count_distribution = defaultdict(lambda: 1)
        # percentage of rows represented by each type of duplicate
        self.percent_distribution = dict()
        # distribution of labels for duplicate record
        self.duplicate_labels = set()
        # dictionary where a tuple of (row_attr, label) maps to
        # counts of distinct response variable (label) values
        self.duplicate_label_counts = defaultdict(lambda: 0)
        # dict mapping a row's values to all its possible labels in the
        # dataframe
        self.row_labels = defaultdict(lambda: set())
        # response variable in training row
        # do not take in consideration identifying duplicates
        # store as single element list to concatenate with lavebel
        self.label = None
        # names of id columns uniqely identifying rows
        # do not take in consideration identifying duplicates
        self.id_col = None
        # reportlab attributes
        self.elements = []
        self.header = ParagraphStyle(1, fontsize=16, styles="BodyText", fontName='Helvetica-Bold')
        self.cell = ParagraphStyle(1, fontsize=13, styles="BodyText", fontName = 'Times')
    """
    READ_INPUT()
    Descr: reads in tab delimited text file, stores it as Pandas Dataframe
    also takes in column names of id column and response var column
    Input: filepath(string), response_var(string), id_col(string)
    """
    def read_input(self, filepath, response_var, id_col):
        #self.df = pandas.read_csv('sample_amelia.txt', delimiter=r"\t+")
        if file != None:
            print "FILEPATH DUPLICATES", filepath
            self.df = pandas.read_csv(filepath, delimiter="\t", quotechar='"')

        self.file = open("duplicate_report.txt", "wb")
        self.doc = SimpleDocTemplate("duplicate_report.pdf", pagesize=letter)
        
        if response_var != None:
            self.label = response_var
        else:
            print "No response variable"
            sys.exit()
        if id_col != None:
            self.id_col = id_col
            self.to_drop = [self.id_col, self.label]
        else:
            self.id_col = None
            self.to_drop = [self.label]
    """
    DUPLICATE_ASSESMENT()
    Descr: 'main' method of module, performing all essential functions in their proper order.
    Called after 'read_input' is called to establish data parameters
    """
    def duplicate_assesment(self):
        if self.id_col != None:
            self.id_checks_txt()
            self.id_checks_pdf()
        self.find_duplicate_rows()
        self.num_dups()
        self.duplicate_set()
        self.percent_dup()
        self.dup_count_distribution()
        self.dup_percent_distribution()
        self.duplicates_diff_labels()
        self.report_txt()
        self.file.close()
    """
    ID_CHECKS_TXT()
    Descr: looks for repeated record ID values on mulitple rows
    implies repeated records rather than multiple observations with identical values
    Output: writes output to txt file 'duplicate_report.txt'
    """
    def id_checks_txt(self):
        ids = set()
        repeated_ids = defaultdict(lambda: 1)
        repeat_id_rows = defaultdict(lambda: set())

        it = self.df.iterrows()
        for index, row in self.df.iterrows():
            if row[self.id_col] not in ids:
                ids.add(row[self.id_col])
            else:
                repeated_ids[row[self.id_col]] += 1
                repeat_id_rows[row[self.id_col]].add(tuple(row))
        if  repeated_ids.values():
            self.file.write("Repeated ID column values for multiple rows:\n\n")
            for key in repeated_ids.keys():
                self.file.write("   "+str(key)+"   " +
                                str(repeated_ids[key]) + " occurences\n")
                if len(repeat_id_rows[key]) > 1:
                    self.file.write("\t"+ str(len(repeat_id_rows[key]) )+" observations afffiliated with ID\n")
                    for row in repeat_id_rows[key]:
                        self.file.write("\t\t"+str(row)+"\n")
            self.file.write("\n")
    """
    ID_CHECKS_PDF()
    Descr: looks for repeated id column values on mulitple rows
    implies repeated records rather than multiple records with identical values
    writes output to pdf file 'duplicate_report.pdf'
    """
    def id_checks_pdf(self):
        ids = set()
        repeated_ids = defaultdict(lambda: 1)
        repeat_id_rows = defaultdict(lambda: set())

        it = self.df.iterrows()
        for index, row in self.df.iterrows():
            if row[self.id_col] not in ids:
                ids.add(row[self.id_col])
            else:
                repeated_ids[row[self.id_col]] += 1
                repeat_id_rows[row[self.id_col]].add(tuple(row))
        if repeated_ids.values():
            self.elements.append(
                Paragraph("Repeated ID column values for multiple rows:", self.header))
            tbl_data = [[key, repeated_ids[key]]
                        for key in repeated_ids.keys()]
            t = Table(tbl_data)
            t.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                   ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
                                   ]))
            self.elements.append(t)
    """
    REPORT_TXT()
    Descr: write duplicate metrics to .txt file
    """
    def report_txt(self):
        self.file.write("Number of unique duplicate records by values (no row IDS or labels included): " +
                        str(len(self.duplicate_records))+"\n\n")
        self.file.write(
            "Collection of observations appearing as duplicates (no row IDs or labels included):\n")
        for rec in self.duplicate_records:
            self.file.write(str(rec)+"\n - " +
                            str(self.count_distribution[rec])+" occurences\n\n")
        if self.percentage_duplicate > self.percent_threshold:
            self.file.write("Number of duplicate records exceeds Threshold:\n")
            self.file.write(" Percent duplicate records: "+str(self.percentage_duplicate) +
                            " Percent Threshold " + str(self.percent_threshold))
        multi_labels = set(
            [key for key in self.row_labels.keys() if len(self.row_labels[key]) > 1])
        self.file.write(
            "Number of unique duplicate records with more than one label/response variable associated: "+str(len(multi_labels))+"\n\n")
        lbl_info = [((row, lbl), self.duplicate_label_counts[(row, lbl)]) for (
            row, lbl) in self.duplicate_label_counts.keys() if row in multi_labels]
        lbl_info = sorted(lbl_info, key=lambda x: x[0])
        for lbl in lbl_info:
            self.file.write("Record: "+str(lbl[0][0])+"\n")
            self.file.write("Labeled: "+str(lbl[0][1])+"\n")
            self.file.write(str(lbl[1]) + " times\n\n")
    """
    REPORT_PDF()
    Descr: write duplicate metrics to .pdf file
    """
    def report_pdf(self):
        self.elements.append(Paragraph("Number of duplicate rows by values (no row IDS or labels included): " +
                                       str(len(self.duplicate_records))+"\n\n", self.header))
        self.elements.append(Paragraph(
            "Collection of observations appearing as duplicate (no row IDs or labels included):\n", self.header))
        tbl_data = [[Paragraph(str(rec), self.cell), self.count_distribution[
            rec]] for rec in list(self.duplicate_records)]
        t = Table(tbl_data)
        t.setStyle(TableStyle([('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                               ('BOX', (0, 0), (-1, -1), 0.25,
                                colors.black), ('ALIGN', (0, 0), (-1, -1), 'CENTER')
                               ]))
        self.elements.append(t)
        multi_labels = set(
            [key for key in self.row_labels.keys() if len(self.row_labels[key]) > 1])
        self.elements.append(Paragraph(
            "Number of unique duplicate records with more than one label/response variable associated: "+str(len(multi_labels))+"\n\n", self.header))
        lbl_info = [((row, lbl), self.duplicate_label_counts[(row, lbl)]) for (
            row, lbl) in self.duplicate_label_counts.keys() if row in multi_labels]
        lbl_info = sorted(lbl_info, key=lambda x: x[0])
        tbl = []
        for lbl in lbl_info:
            rec = Paragraph(str(lbl[0][0])+"\n", self.cell)
            label = Paragraph(str(lbl[0][1])+"\n", self.cell)
            num = Paragraph(str(lbl[1]), self.cell)
            tbl.append([rec, label, num])

        if self.percentage_duplicate > self.percent_threshold:
            self.elements.append(
                Paragraph("Number of duplicate records exceeds Threshold:\n", self.header))
            self.elements.append(Paragraph(" Percent duplicate records: "+str(
                self.percentage_duplicate)+" Percent Threshold " + str(self.percent_threshold), self.header))

    """
    DUPLICATES_DIFF_LABELS()
    Descr: iterates through our collection of duplicate rows to check two things:
     1) create a set mapping from a row's values to all possible labels
     2) collect the counts of a row's values to each of its possible labels
    """
    def duplicates_diff_labels(self):
        row_values = self.dup_rows
        rows_nolabel = self.dup_rows.drop(self.to_drop, 1)
        for ind, row in rows_nolabel.iterrows():
            lbl = self.dup_rows.get_value(ind, self.label)
            row_key = tuple(row)
            if lbl not in self.row_labels[row_key]:
                self.row_labels[row_key].add(lbl)
            self.duplicate_label_counts[(row_key, lbl)] += 1
    """
    NUM_DUPS()
    Descr:  Count the number of rows in our total DataFrame and in our Duplicates Collection DataFrame
    """
    def num_dups(self):
        self.total_rows = len(self.df)
        self.num_dup_rows = len(self.dup_rows)


    """
    DUPLICATE_SET
    Descr:  Iterate through our collectin of duplicates and strip it down to a set
    of unique rows of values (ie - record IDS and response variables are not included)
    """
    def duplicate_set(self):
        rows_no_labels = self.dup_rows.drop(self.to_drop, 1)
        for index, row in rows_no_labels.iterrows():
            if tuple(row) not in self.duplicate_records:
                self.duplicate_records.add(tuple(row))

    """
    PERCENT_DUP()
    Descr: Computes what percentage of our total DataFrame are composed of duplicate records
    """
    def percent_dup(self):
        self.percentage_duplicate = float(
            self.num_dup_rows) / self.total_rows
    """
    DUP_COUNT_DISTRIBUTION()
    Descr: Iterate through our collectin of duplicate rows and count how often
    an instance of a row's values occur.
    """
    def dup_count_distribution(self):
        rows_no_labels = self.dup_rows.drop(self.to_drop, 1)
        rows = rows_no_labels.values
        for i in range(len(rows)-1):
            if self.equal_rows(rows[i], rows[i+1]):
                self.count_distribution[tuple(rows[i])] += 1

    """
    DUP_PERCENT_DISTRIBUTION()
    Descr: Get the percentage occurence of each duplicate record in our total DataFrame
    """
    def dup_percent_distribution(self):
        for record in self.count_distribution.keys():
            self.percent_distribution[record] = float(
                self.count_distribution[record])/self.total_rows
    """
    FIND_DUPLICATE_ROWS()
    Descr: Create a subset DataFrame consisting of all duplicate records
    records/rows are compared across all columns/varaibles except for the response var/label
    """
    def find_duplicate_rows(self):
        # get all the columns for comparison without the Class Label(s) and Row
        # IDs
        columns_to_compare = self.df.columns
        columns_to_compare = columns_to_compare.drop(
            self.to_drop)
        # search for duplicates and return this subset of repeated rows
        dups = self.df[self.df.duplicated(
            subset=columns_to_compare, keep=False)]
        # sort collection by our columns to compare
        dups = dups.sort_values(by=columns_to_compare.get_values().tolist())
        # set them to our object's member 'dup_rows'
        self.dup_rows = dups

    """
    EQUAL ROWS()
    Descr: takes in two numpy arrays representing rows of our Dataframe
    Input: r1 (numpy array), r2 (numpy array)
    Output: Bool
    """
    def equal_rows(self, r1, r2):
        return np.array_equal(r1, r2)

if __name__ == '__main__':
    dup_inf = duplicate_info()
    dup_inf.read_input("sample_amelia.txt", "is_valid_flag",
                       "AmeliaID", "yzyz")
    dup_inf.duplicate_assesment()
    # dup_inf.canvas.save()
    dup_inf.doc.build(dup_inf.elements)
