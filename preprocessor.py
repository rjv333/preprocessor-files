import cleansing
from cleansing import *
import duplicate_report
from duplicate_report import *

import diagnostictasks
from diagnostictasks import *


import imputation
from imputation import *

class preprocessor:
	def __init__(self):
		self.duplicate_reporter = duplicate_info()
		self.data_cleanser = data_cleanser()

	"""
	PREPROCESS_PATH1()
	Descr:  Longer of two paths.  Contains rows 2-11,13 of IP Analytics Task Sheet
	Input: raw_data_path(string): location of data file, resp_var (string) identifies response variable,
		id_col(string): identifies column of record ID (optional)
	"""
	def preprocess_path1(self, raw_data_path, resp_var, id_col):
		# row 2 - report on types
		# row 3 - check uniformity of field lengths
		# row 4 - flag fields with no variance
		# row 6 - report distributions of variables, draw histograms
		initial_diagnostics(raw_data_path)

		# row 5 - asses duplicates
		self.duplicate_reporter.read_input(raw_data_path, resp_var, id_col)
		self.duplicate_reporter.duplicate_assesment()

		# row 7 - drop cols with no variance, and categorical vars with too many vals
		# row 9 - drop rows with repeated record IDs
		# row 8 -  drop cols with excessive missing values
		# row 10 - drop rows with excessive missing variables
		self.data_cleanser.read_input(raw_data_path, id_col)
		self.data_cleanser.clean_data_path1()


		# row 13 - Imputed missing variables with kNN
		self.data_transfrmer = data_transformer("cleansed_matrix.txt")
		self.data_transfrmer.impute_features()

		# row 11 - relabel categorical variables with dummy coding
		self.data_transfrmer.categorical_relabel()
	"""
	PREPROCESS_PATH2()
	Descr:  Shorter of two possible paths.  Contains rows 2-6,9of IP Analytics Task Sheet
	Input: raw_data_path(string): location of data file, resp_var (string) identifies response variable,
		id_col(string): identifies column of record ID (optional)
	"""
	def preprocess_path2(self, raw_data_path, resp_var, id_col):
		# row 2 - report on types
		# row 3 - check uniformity of field lengths
		# row 4 - flag fields with no variance
		# row 6 - report distributions of variables, draw histograms
		initial_diagnostics(raw_data_path)

		# row 5 - asses duplicates
		self.duplicate_reporter.read_input(raw_data_path, resp_var, id_col)
		self.duplicate_reporter.duplicate_assesment()

		# row 9 - drop rows with repeated record IDs
		self.data_cleanser.read_input(raw_data_path, id_col)
		self.data_cleanser.clean_data_path2()


if __name__ == '__main__':
	p = preprocessor()
	#p.preprocess_path1("pima-indians-diabetes_mod.data","Class",None)
	#p.preprocess_path1("ab_mod_sample.txt","shell_weight","ID")
	#p.preprocess_path2("ab_mod_sample.txt","shell_weight","ID")
	#p.preprocess_path1("ab_sample.txt","shell_weight","ID")
	#p.preprocess_path1("samp_amelia.txt","is_valid_flag","AmeliaID")
	#p.preprocess_path1("ipsoft_sampled_cases_copy.csv","ctgry_name",None)
	#p.preprocess_path1("ipsoft_cases_to_surveys_tabs.csv", "fcr_response_score", "session_id")
	#p.preprocess_path1("ipsoft_session_tabs.csv", "player_id_type", "player_id")
	p.preprocess_path1("ipsoft_sampled_cases_tabs.csv", "ctgry_name", "account_key")


