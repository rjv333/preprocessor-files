import pandas as pd
import numpy as np
import time
#from geopy.distance import vincenty
from dateutil.parser import parse as date_parse


class cleansing_conversion:
    """
    CONSTRUCTOR
    Input: df (Pandas Dataframe)
    """
    def __init__(self, df):
        self.pd = df
        self.matform = np.asmatrix(df)
        self.row = self.matform.shape[0]
        self.col = self.matform.shape[1]
    """
    CONVERT_DATE_COL()
    Descr: Iterates over columns of DataFrame, and checks if a column contains dates/timestamps
        If so, that column has its data converted to 'epoch seconds'
    """
    def convert_date_col(self):
        date_columns = set()
        for i in self.pd:
            if is_date(self.pd[i][1]):
                date_columns.add(i)
                self.pd[i] = self.datetimetoepochseconds(self.pd[i])
        return date_columns

    """
    DATETIME TO EPOCH SECONDS()
    Descr: Iterates over the data of a datetime column, 
        creates and returns a new column with date in epoch seconds
    Input: date_col (Series ), column of datetime values
    """
    def datetimetoepochseconds(self, date_col):
        epochtimevalues = []
        dates = date_col.tolist()
        for a in dates:
            #check for missing values
            if is_date(a):
                datevalue = date_parse(a)
                epochseconds = time.mktime(datevalue.timetuple())
                epochtimevalues.append(epochseconds)
            else:
                epochtimevalues.append(epochseconds)
        return pd.Series(epochtimevalues)

"""
IS_DATE
Descr: tests a value to see if it can be parsed as a date
Input: val (possibly a datetime string, presumably an object(string))
"""
def is_date(val):
    if type(val) == type("string") and len(val) < 3:
        return False
    if type(val) == type("string"):
        try:
            date_parse(val)
            return True
        except:
            return False

if __name__ == "__main__":
    pda = pd.read_csv('samp_amelia.txt',delimiter = '\t')
    cc = cleansing_conversion(pda)
    print pda["adv_chat_start_tms_cst_dttm"]
    cc.convert_date_col()
    print cc.pd["adv_chat_start_tms_cst_dttm"]

