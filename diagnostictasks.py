import pandas as pd
import numpy as np
import pylab as plab
import matplotlib.pyplot as plt
import sys
from matplotlib.backends.backend_pdf import PdfPages
import math

reload(sys)
sys.setdefaultencoding("utf-8")

'''
Function name: initial_diagnostics(filename)
Descr:  reading the input file and converting to matrix format. It calls three functions; each function depicting a task.
Calls   datatypes(pandas data frame), shapeofdata( row, column) and findfrequency( row, col, headers of data, pdf file to write output to)

Input:  filename which should be in .txt and contains tab separated values.
Output: Calls all the three diagnostic functions.
'''
def initial_diagnostics(filename):
    pda = pd.read_csv(filename, delimiter='\t', quotechar='"')
    # pdacopy = pda
    # pdacopy = pdacopy.dropna()
    # header stores the column names of all the columns of the data.
    header = []
    for column in pda:
        header.append(column)
    # Pandas dataframe converted to matrix.
    matform = np.asmatrix(pda)
    row, col = matform.shape
    # features = matform[:, 0:col-1]
    # classval = matform[:, col-1].tolist()
    pp = PdfPages("diagnosticplots.pdf")
    # Call the methods.
    datatypes(pda)  # task 1
    shapeofdata(row, col)  # Task 2
    findfrequency(row, col, matform, header, pp)  # task 3
    pp.close()
    plt.close('all')

'''
Function name: datatypes(pandas data frame)
Descr:  Determins the data type of each of the column present in the TSV file. Also, it writes the output to text file.
Input:  pandas data frame.
Output: Write output to 'dtypereport.txt' in string format.
'''
def datatypes(pda):
    try:
        dtypevalues = pda.dtypes
        print(dtypevalues)
        reportdtype = open('dtypereport.txt', 'wb')
        reportdtype.write(str(dtypevalues))
        reportdtype.close()
    except:
        # REPORT
        e = sys.exc_info()[0]
        errordtype = open('ERR_Dtype.txt', 'wb')
        errordtype.write(str(e))

'''
Function name: shapeofdata(row, col)
Descr:  Determines the dimension of the data. If shape is proper, it returns the shape but if shape is irregular, it
        returns an error.
Input:  It takes upper limit of row and column of the data frame as input
Output: If shape is proper, Writes the dimension to 'DimensionofData.txt' file.
        If shape is improper, writes the error to 'ERR_dimension.txt' file.
'''
def shapeofdata(row, col):
    try:
        dimension = [row, col]
        reportshape = open('DimensionofData.txt', 'wb')
        reportshape.write(str(dimension))
        reportshape.close()
    except:
        e = sys.exc_info()[0]
        errorshape = open('ERR_dimension.txt', 'wb')
        errorshape.write(str(e))
        sys.exit(0)

'''
Function name: findfrequency(row, column, data matrix, header containing the name of each column, pdfwriter)
Descr:  Calculates frequency of occurence of each value in each column which is later used for generating the
        histogram. It checks the datatype of the column and based on that generates the histogram.
            If data type == Long, Int or float: Creates bin and generates histogram
            If data type == string, obj: Creates histogram for each category.
Input:  row, column, data in matrix format (Numpy matrix), header containing label for each column, pdfwriter)
Output: Calls plthist(column label, frequency (dictionary), pdwriter) and histogram(column label, frequency (dictionary)
        , pdfwriter) based on the data type.
'''
def findfrequency(row, col, mat, header, pp):
    count = 0
    for eachcol in range(col):
        try:
            frequency = {}
            dtype = ''
            iter = 0
            for value in mat[:, eachcol]:
                value = value.tolist()
                for v in value:
                    if iter is 0:
                        for a in v:
                            dtype = type(a)
                            if a in frequency:
                                frequency[a] += 1
                            else:
                                frequency[a] = 1

            FeatureName = header[count]
            count += 1
            if dtype == long or dtype == int or dtype == float:
                plthist(FeatureName, frequency, pp)
            else:
                histogram(FeatureName, frequency, pp)
        except:
            print "error analyzing frequency of col", eachcol

'''
Function name: histogram(column label, frequency (dictionary), pdfwriter)
Descr:  This generates the histogram for each column which have categorical values.
Input:  Takes in column name, frequency (dictionary) containing each value and count of occurence of that value,
        pdfwriter where all the results are written to.
Output: Histogram written to PDF named 'diagnosticplots.pdf'
'''
def histogram(featurename, frequency, pp):
    length = np.arange(len(frequency))
    lvalues = []
    lkeys = []
    for k, v in frequency.iteritems():
        if v > 1:
            lvalues.append(v)
            lkeys.append(k)

    # Maximum value of y axis is max value of dictionary added to 50.
    if max(frequency.values()) < 50:
        max_Value = max(frequency.values()) * 2
    else:
        max_Value = max(frequency.values()) + 100

    plab.figure()
    plab.bar(length, frequency.values(), width=1.0, align='center')
    plab.xticks(length, frequency.keys())
    plab.xlabel(featurename)
    plab.ylabel('Frequency')
    plab.ylim(0, max_Value)
    plab.title('Histogram for ' + featurename)
    pp.savefig()

'''
Function name:  plthist(column name, frequency (dictionary), pdfwriter)
Descr:  Plots histogram for each column which has numeric values.
Input:  column name, frequency (dictionary) Which has each value with count of occurence and pdfwriter to which the
        output is written to.
Output: Plots are written to 'diagnosticplots.pdf'
'''

def plthist(featurename, frequency, pp):
    plt.figure()
    keys = [x for x in frequency.keys() if str(x) != 'nan']
    plt.hist(keys)
    plt.ylabel('Frequency')
    plt.xlabel(featurename)
    plt.title('Histogram for '+featurename)
    pp.savefig()

if __name__ == "__main__":
    read_File()