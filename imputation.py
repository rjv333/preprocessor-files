import pandas

import duplicate_report
from duplicate_report import *
import re

from dateutil.parser import parse as date_parse

import re
from re import match


import subprocess

"""
 DATA TRANSFORMER CLASS
 performs final two preprocessor actions
 ROW 13 - Impute the values of missing features with kNN
 ROW 11 - DUMMY CODE categorical values
"""
class data_transformer:
	def __init__(self, filepath):
		self.filepath = filepath
		self.pandas_df = pandas.read_csv(filepath, sep="\t", quotechar='"')
	"""
	IMPUTE FEATURES
	Descr: Impute missing values with R VIM pkg's kNN
	 k = 10 when n > 500 ,else 5
	 create 'imputed_df' as pandas dataframe
	"""
	def impute_features(self):

		n = self.pandas_df.shape[0]
		if n >=500:
			k="10"
		else:
			k="5"

		knn_R_script(self.filepath, k)
		imputed_df = pandas.read_csv("r_imputed_mid_format.txt",sep="\t")
		mid = len(imputed_df.columns)/2 + 1
		imputed_df = imputed_df.drop(imputed_df.columns[mid:],1)
		self.pandas_df = imputed_df
		self.pandas_df = imputed_df.drop(imputed_df.columns[0],1)
		self.pandas_df.to_csv("imputed_matrix.txt", sep="\t", mode='w', index=False)
	"""
	CATEGORICAL_RELABEL()
	Descr: Create a transformed matrix of our dataframe by applying dummy coding to 
		preselected categorical columns
	"""
	def categorical_relabel(self):
		to_dummy = self.dummy_columns()
		self.transformed_df = pandas.get_dummies(self.pandas_df, columns = to_dummy)
		self.transformed_df.to_csv("transformed_matrix.txt",sep="\t", mode='w', index=False)
	"""
	DUMMY_COLUMNS()
	Descr: Iterate over the columns of our DataFrame to find which variables are eligible for
		'dummy coding' (conversion of categorical to qunatitative values)
	"""
	def dummy_columns(self):
		nrows = self.pandas_df.shape[0]
		thresh = nrows * .95
		categorical_thresh = thresh * .4
		#num_thresh = nrows *
		col_indexes_not_to_dummy_code = []
		for i in self.pandas_df.ix[0:,]:
			if ( (self.pandas_df[i].nunique() > thresh) or ((type(self.pandas_df[i][0]) == type("string")) and (self.pandas_df[i].nunique()  > categorical_thresh)) or
				(type(self.pandas_df[i][0]) != type("string")) or (other_continuous_value(self.pandas_df[i][0]) ) ):
				col_indexes_not_to_dummy_code.append(i)
		res = set(self.pandas_df.columns).difference(set(col_indexes_not_to_dummy_code))
		return res

"""
kNN_R_SCRIPT()
Descr: Runs R script as a subprocess for the imputation of missing values through kNN
Input: filename (string)- of matrix to impute, k(int)- number of neighbors used in kNN
"""
def knn_R_script(filename, k):
    idfile = filename  
    retval = subprocess.check_output(["Rscript", "kNN_imputation.R", idfile, k])
    with open("r_imputed.txt",'r') as f, open("r_imputed_mid_format.txt",'w') as f1:
        next(f) # skip header line
        for line in f:
            f1.write(line)
    return 


def other_continuous_value(val):
	if val[0] and val[len(val)-1] == '"':
		val = val[1:-1]
	return (is_isp(val) or is_date(val) or is_number(val))
def is_isp(val):
	return bool(re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",val))
def is_date(val):
	if len(val) < 4:
		return False
	try:
		 date_parse(val)
		 return True
	except:
		return False
def is_number(val):
    try:
        float(val)
        return True
    except ValueError:
        pass
 
    try:
        import unicodedata
        unicodedata.numeric(val)
        return True
    except (TypeError, ValueError):
        pass

	return False



if __name__ == '__main__':

	#df = pandas.read_csv("air.txt", delimiter=r"\t+")
	knn_R_script("air.txt","5")
	#dt = data_transformer("air.txt")
	#dt.impute_features()
	#dt.categorical_relabel()
