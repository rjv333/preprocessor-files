import pandas
import numpy as np
from collections import defaultdict
from collections import Counter
import cleansing_conversion
from cleansing_conversion import *
import sys
import math


empty_values = {None, "", "NA", 'Null', 'NULL', ''}
#THRESHOLD FOR THE NUMBER OF POSSIBLE VALUES A CATEGORICAL VARIABLE CAN HAVE IN OUT DATA SET
categorical_thresh = 10




class data_cleanser:
    """
    CONSTRUCTOR
    Descr: Initializes attributes of 'data_cleanser' class
    """
    def __init__(self):
        df = None
        self.id_col = None
        self.missing_threshold = .3
        self.missing_vars = set()
        self.no_variance = set()
        self.categorical_vars_with_too_many_values = set()
        self.repeated_obs = defaultdict(lambda: 1)
        self.rows_missing_values = set()
    """
    READ_INPUT()
    Descr: reads data input and creates Pandas DataFrame representation
    Input: filpath (string): location of data input , id_col(string) : name of column identifying response var
    """
    def read_input(self, filepath, id_col):
        self.df = pandas.read_csv(filepath, delimiter="\t", quotechar='"')
        if id_col != None:
            self.id_col = id_col
        self.file = open(r"cleansing_report.txt", "wb")

    """
    CLEANSING_CONVERSIONS()
    Descr: revises the format of certain data columns
    namely, timestamps are converted to int value of epochtimestamp
    """
    def cleansing_conversions(self):
        cc = cleansing_conversion(self.df)
        date_col = cc.convert_date_col()
        self.df = cc.pd
        return date_col

    """
    CLEAN_DATA_PATH1()
    Descr: The more elaborate of our two possible paths for data cleansing
    We drop variables with no variance or excessive categorical variance, 
    then drop duplicate record rows, then drop variables with excessive missing vals,
    and then 
    Input: None (parameters already loaded from 'read_input')
    Output: Written output of cleansed data sent to 'cleansed_matrix.txt'
    """
    def clean_data_path1(self):
        date_col = self.cleansing_conversions()
        col_to_clean = set(self.df.columns) - date_col
        # row 7
        #for i in range(len(self.df.columns)):
            #self.inspect_variance(self.df.ix[:, i])
        for col in col_to_clean:
            self.inspect_variance(self.df[col])
        self.drop_no_variance()
        self.drop_categorical_vars_with_too_many_values()
        # row 9
        if self.id_col != None:
            self.drop_duplicate_rows()
            self.drop_ID_col()
        # row 8
        for i in range(len(self.df.columns)):
            self.find_missing_vars(self.df.ix[:, i])
        self.drop_missing_vars()
        # row 10
        self.drop_rows_with_missing_values()
        # report mods
        self.report_cleansing_path1()
        self.file.close()
        self.df.to_csv("cleansed_matrix.txt",mode='w',index=False, sep="\t")
    """
    CLEAN_DATA_PATH2()
    Descr: Simpler path of data cleansing.  Simply drop duplcate records
    Input: None (parameters already loaded from 'read_input')
    Output: Written output of cleansed data sent to 'cleansed_matrix.txt'
    """    
    def clean_data_path2(self):
        self.cleansing_conversions()
        # row 9 
        if self.id_col != None:
            self.drop_duplicate_rows()
            self.drop_ID_col()
        # report mods
        self.report_cleansing_path2()
        self.file.close()
        self.df.to_csv("cleansed_matrix.txt",mode='w',index=False, sep="\t")

    """
    FIND_MISSING_VARS()
    Descr: Row 8 of IP Analytics Tasks.
    Check if variable column has excessive missing values and should be dropped
    Input: column (Series datatype)
    """
    def find_missing_vars(self, column):
        empty_count = 0
        for ind, val in column.iteritems():
            if type(val) != type("string") and np.isnan(val):
                empty_count += 1
            elif val in empty_values:
                empty_count += 1
        if float(empty_count)/column.size > self.missing_threshold:
            self.missing_vars.add(column.name)
    """
    DROP MISSING VARS()
    Descr: Drops all columns from DataFrame that have been deemed to have 
    to many missing values
    """
    def drop_missing_vars(self):
        for col in self.missing_vars:
            self.df = self.df.drop(col,1)
    """
    INSPECT_VARIANCE()
    Descr: Row 7 of IP Analytics Tasks. 
    Check if a column should be discarded for satisfying one of two conditions
        1) Column has no variance (and can therefore offer no insights)
        2) Column is of categorical nature and has too many possible values for use
    Input: column (Series) represents a variable of our data set
    """
    def inspect_variance(self, column):
        if column.nunique() == 1:
            self.no_variance.add(column.name)
        if str( column.dtype) == 'object' and column.nunique() > categorical_thresh and column.name != self.id_col:
            self.categorical_vars_with_too_many_values.add((column.name, column.nunique()))
    """
    DROP_NO_VARIANCE()
    Descr: Drops all columns from DataFrame that have no variance
    """
    def drop_no_variance(self):
        for col in self.no_variance:
            self.df = self.df.drop(col,1)
    """
    DROP_CATEGORICAL_VARS_WITH_TOO_MANY_VALUES()
    Descr: Drops all categorical columns from DataFrame that have excessive variance
    """
    def drop_categorical_vars_with_too_many_values(self):
        for col, count in self.categorical_vars_with_too_many_values:
            self.df = self.df.drop(col,1)
    """
    DROP_DUPLICATE_ROWS()
    Descr: Row 9 of IP Analytics Tasks.  Find duplicate rows and remove them from Pandas DataFrame
    Only called if self.id_col != 'None' so we can assume duplicates represent repeat observations
    and not seperate but identical observations
    """
    def drop_duplicate_rows(self):
        obs = set()
        it = self.df.iterrows()
        for index, row in self.df.iterrows():
            if tuple(row) not in obs:
                obs.add(tuple(row))
            else:
                self.repeated_obs[tuple(row)] += 1
        self.df = self.df.drop_duplicates()
    """
    DROP_ID_COL()
    Descr: After checking for repeated observations to cleanse, we can discard observation ID's
    """
    def drop_ID_col(self):
        self.df = self.df.drop(self.id_col,1)
    """
    DROP_ROWS_WITH_MISSING_VALUES()
    Descr: Row 10 of IP Analytics Tasks.  Find records with > 30 percent missing values
    and drop them from our dataframe
    """
    def drop_rows_with_missing_values(self):
        for index, row in self.df.iterrows():
            row_count = 0
            for i in range(row.size):
                if type(row[i]) != type("string") and np.isnan(row[i]):
                    row_count += 1
                elif row[i] in empty_values:
                    row_count += 1
            if float(row_count)/row.size > .3:
                self.rows_missing_values.add(tuple(row))
                self.df = self.df.drop(index,0)
    """
    REPORT_CLEANSING_PATH1()
    Descr: Report on the dropped columns and rows as pertains to those steps taken in 
        cleansing path 1
    """
    def report_cleansing_path1(self):
        self.file.write("SUMMARY OF COLUMNS AND ROWS REMOVED FROM DATASET:\n\n")
        self.file.write("Variables with missing values in excessive instances: "+str( len(self.missing_vars) )+" columns\n")
        self.file.write("\nVariables with no variance: "+str(len(self.no_variance))+" columns\n")
        self.file.write("\nCategorical variables with excessive variance: "+str(len(self.categorical_vars_with_too_many_values))+" columns\n")
        self.file.write("\nRows with repeated record IDs: "+ str(sum(self.repeated_obs.values()) - len(self.repeated_obs.keys()))+" extra rows\n")
        self.file.write("\nRows with excessive missing values: "+str(len(self.rows_missing_values))+" rows" )

        self.file.write("\n\nELABORATION OF DELETED COLUMNS AND ROWS\n\n")
        self.file.write("Variables with missing values in excessive instances: "+str( len(self.missing_vars) )+" columns\n")
        for var in self.missing_vars:
            self.file.write( "     "+ str(var)+"\n" )
        self.file.write("\nVariables with no variance: "+str(len(self.no_variance))+" columns\n\n")
        for var in self.no_variance:
            self.file.write("     "+str(var)+"\n")
        self.file.write("\nCategorical variables with excessive variance: "+str(len(self.categorical_vars_with_too_many_values))+" columns\n")
        for var, count in self.categorical_vars_with_too_many_values:
            self.file.write("     "+str(var)+": "+str(count)+" unique values\n")            
        self.file.write("\nRows representing duplicate observations: "+ str(sum(self.repeated_obs.values()) - len(self.repeated_obs.keys()))+" extra rows\n\n")
        for row_id in self.repeated_obs:
            self.file.write("     "+str(row_id)+" "+str(self.repeated_obs[row_id]-1)+" extra instances\n")
        self.file.write("\nRows with excessive missing values: "+str(len(self.rows_missing_values))+" rows\n" )
        for ind in self.rows_missing_values:
            self.file.write(self.id_col+": "+str(ind)+"\n")

    """
    REPORT_CLEANSING_PATH2()
    Descr: Report on the dropped columns and rows as pertains to those steps taken in 
        cleansing path 2
    """
    def report_cleansing_path2(self):
        self.file.write("\nRows Removed with repeated record IDs: "+ str(sum(self.repeated_obs.values()) - len(self.repeated_obs.keys()))+" extra rows\n\n")
        for observation in self.repeated_obs:
            self.file.write("     "+str(observation)+" "+str(self.repeated_obs[observation]-1)+" extra instances\n")


if __name__ == '__main__':
    c = clean_info()
    c.read_input("sample_amelia.txt", "AmeliaID", "sample")
    c.clean_data_path1()
